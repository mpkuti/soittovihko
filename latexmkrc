# TeX -> PDF
$pdf_mode = 1;

# THE MAIN FILE
@default_files = ('soittovihko.tex');

# OUTPUT DIRECTORY
$out_dir = 'output';
